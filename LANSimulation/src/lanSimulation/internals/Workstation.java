package lanSimulation.internals;

public class Workstation extends Node {

	public Workstation(String _name) {
		super(_name);
	}
	
	public Workstation(String _name, Node _nextNode) {
		super(_name, _nextNode);
	}
}
