package lanSimulation.internals;

import java.io.IOException;
import java.io.Writer;

public class Printer extends Node {

	public Printer(String _name) {
		super(_name);
	}
	
	public Printer(byte _type, String _name, Node _nextNode) {
		super(_name, _nextNode);
	}
	
	public boolean printJobStatus(Packet document, Writer report) {
		String author = "Unknown";
		String title = "Untitled";
		int startPos = 0, endPos = 0;
		try {
			if (document.getMessage().startsWith("!PS")) {
				startPos = document.getMessage().indexOf("author:");
				if (startPos >= 0) {
					endPos = document.getMessage().indexOf(".", startPos + 7);
					if (endPos < 0) {
						endPos = document.getMessage().length();
					}

					author = document.getMessage().substring(startPos + 7,
							endPos);
				}

				startPos = document.getMessage().indexOf("title:");
				if (startPos >= 0) {
					endPos = document.getMessage().indexOf(".", startPos + 6);
					if (endPos < 0) {
						endPos = document.getMessage().length();
					}
					title = document.getMessage()
							.substring(startPos + 6, endPos);
				}

				report.write("\tAccounting -- author = '");
				report.write(author);
				report.write("' -- title = '");
				report.write(title);
				report.write("'\n");
				report.write(">>> Postscript job delivered.\n\n");
				report.flush();
			} else {
				title = "ASCII DOCUMENT";
				if (document.getMessage().length() >= 16) {
					author = document.getMessage().substring(8, 16);
				}
				
				report.write("\tAccounting -- author = '");
				report.write(author);
				report.write("' -- title = '");
				report.write(title);
				report.write("'\n");
				report.write(">>> ASCII Print job delivered.\n\n");
				report.flush();
			}

		} catch (IOException exc) {
			// just ignore
		}
		return true;
	}
}
