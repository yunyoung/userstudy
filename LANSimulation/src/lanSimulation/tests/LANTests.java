/*   This file is part of lanSimulation.
 *
 *   lanSimulation is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   lanSimulation is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with lanSimulation; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 *   Copyright original Java version: 2004 Bart Du Bois, Serge Demeyer
 *   Copyright C++ version: 2006 Matthias Rieger, Bart Van Rompaey
 */
package lanSimulation.tests;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import lanSimulation.LANSimulation;
import lanSimulation.Network;
import lanSimulation.internals.Node;
import lanSimulation.internals.Packet;

public class LANTests extends TestCase {

	public static Test suite() {
		TestSuite testSuite = new TestSuite(LANTests.class);
		return testSuite;
	}

	public void testBasicPacket() {
		Packet packet;

		packet = new Packet("c", "a");
		assertEquals("message", "c", packet.getMessage());
		assertEquals("destination", "a", packet.getDestination());
		assertEquals("origin", "", packet.getOrigin());
		packet.setOrigin("o");
		assertEquals("origin (after setting)", "o", packet.getOrigin());
	}

	@SuppressWarnings("resource")
	private boolean compareFiles(String filename1, String filename2) {
		FileInputStream f1 = null, f2 = null;
		int b1 = 0, b2 = 0;

		try {
			f1 = new FileInputStream(filename1);
			f2 = new FileInputStream(filename2);
			
			if (f1.available() != f2.available()) {
				return false;
			} // length of files is different
			
			while ((b1 != -1) & (b2 != -1)) {
				b1 = f1.read();
				b2 = f2.read();
				if (b1 != b2) {
					return false;
				} // discovered one differing character
			}

			if ((b1 == -1) & (b2 == -1)) {
				return true; // reached both end of files
			} else {
				return false; // one end of file not reached
			}
		} catch (Exception f1exc) {
			return false;
		} finally {
			try {
				f1.close();
				f2.close();
			} catch (IOException exc) {
			}
		}
	}

	public void testBasicNode() {
		Node node = new Node("n");
		assertEquals("type", Node.class, node.getClass());
		assertEquals("name", "n", node.getName());
		assertEquals("nextNode", null, node.getNextNode());
		node.setNextNode(node);
		assertEquals("nextNode (after setting)", node, node.getNextNode());
	}

	public void testDefaultNetworkToString() {
		Network network = LANSimulation.DefaultExample();
		assertTrue("consistentNetwork ", network.consistentNetwork());
		assertEquals("DefaultNetwork.toString()", "Workstation Filip [Workstation] -> Node n1 [Node] -> Workstation Hans [Workstation] -> Printer Andy [Printer] ->  ... ", network.toString());
	}

	public void testWorkstationPrintsDocument() {
		Network network = LANSimulation.DefaultExample();
		StringWriter report = new StringWriter(500);

		assertTrue("PrintSuccess ", network.requestWorkstationPrintsDocument("Filip", "Hello World", "Andy", report));
		assertFalse("PrintFailure (UnkownPrinter) ", network.requestWorkstationPrintsDocument("Filip", "Hello World", "UnknownPrinter", report));
		assertFalse("PrintFailure (print on Workstation) ", network.requestWorkstationPrintsDocument("Filip", "Hello World", "Hans", report));
		assertFalse("PrintFailure (print on Node) ", network.requestWorkstationPrintsDocument("Filip", "Hello World", "n1", report));
		assertTrue("PrintSuccess Postscript", network.requestWorkstationPrintsDocument("Filip", "!PS Hello World in postscript", "Andy", report));
		assertFalse("PrintFailure Postscript", network.requestWorkstationPrintsDocument("Filip", "!PS Hello World in postscript", "Hans", report));
	}

	public void testBroadcast() {
		Network network = LANSimulation.DefaultExample();
		StringWriter report = new StringWriter(500);
		assertTrue("Broadcast ", network.requestBroadcast(report));
	}

	/**
	 * Test whether output routines work as expected. This is done by comparing
	 * generating output on a file "useOutput.txt" and comparing it to a file
	 * "expectedOutput.txt". On a first run this test might break because the
	 * file "expectedOutput.txt" does not exist. Then just run the test, verify
	 * manually whether "useOutput.txt" conforms to the expected output and if
	 * it does rename "useOutput.txt" in "expectedOutput.txt". From then on the
	 * tests should work as expected.
	 */
	public void testOutput() {
		Network network = LANSimulation.DefaultExample();
		String generateOutputFName = "useOutput.txt", expectedOutputFName = "expectedOutput.txt";
		FileWriter generateOutput;
		StringWriter report = new StringWriter(500);

		try {
			generateOutput = new FileWriter(generateOutputFName);
		} catch (IOException f2exc) {
			assertTrue("Could not create '" + generateOutputFName + "'", false);
			return;
		}

		try {
			report.write("\n\n---------------------------------SCENARIO: Print Success --------------------------\n");
			network.requestWorkstationPrintsDocument("Filip", "Hello World", "Andy", report);
			report.write("\n\n---------------------------------SCENARIO: PrintFailure (UnkownPrinter) ------------\n");
			network.requestWorkstationPrintsDocument("Filip", "Hello World", "UnknownPrinter", report);
			report.write("\n\n---------------------------------SCENARIO: PrintFailure (print on Workstation) -----\n");
			network.requestWorkstationPrintsDocument("Filip", "Hello World", "Hans", report);
			report.write("\n\n---------------------------------SCENARIO: PrintFailure (print on Node) -----\n");
			network.requestWorkstationPrintsDocument("Filip", "Hello World", "n1", report);
			report.write("\n\n---------------------------------SCENARIO: Print Success Postscript-----------------\n");
			network.requestWorkstationPrintsDocument("Filip", "!PS Hello World in postscript", "Andy", report);
			report.write("\n\n---------------------------------SCENARIO: Print Failure Postscript-----------------\n");
			network.requestWorkstationPrintsDocument("Filip", "!PS Hello World in postscript", "Hans", report);
			report.write("\n\n---------------------------------SCENARIO: Broadcast Success -----------------\n");
			network.requestBroadcast(report);
			generateOutput.write(report.toString());
		} catch (IOException exc) {
			// just ignore
		} finally {
			try {
				generateOutput.close();
			} catch (IOException exc) {
				// just ignore
			}
		}

		assertTrue("Generated output is not as expected ", compareFiles(generateOutputFName, expectedOutputFName));
	}
	
	private static final String SimpleOutput = "Workstation Filip [Workstation] -> Node n1 [Node] -> Workstation Hans [Workstation] -> Printer Andy [Printer] ->  ... ";
	private static final String HTMLOutput = "<HTML>\n<HEAD>\n<TITLE>LAN Simulation</TITLE>\n</HEAD>\n<BODY>\n<H1>LAN SIMULATION</H1>\n\n<UL>\n\t<LI> Workstation Filip [Workstation] </LI>\n\t<LI> Node n1 [Node] </LI>\n\t<LI> Workstation Hans [Workstation] </LI>\n\t<LI> Printer Andy [Printer] </LI>\n\t<LI>...</LI>\n</UL>\n\n</BODY>\n</HTML>\n";
	private static final String XMLOutput = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\n<network>\n\t<Workstation>Filip</Workstation>\n\t<Node>n1</Node>\n\t<Workstation>Hans</Workstation>\n\t<Printer>Andy</Printer>\n</network>";
	
	public void testPrint() {
		
	}

	public void testPrintHTML() {
		
	}

	public void testPrintXML() {

	}
}